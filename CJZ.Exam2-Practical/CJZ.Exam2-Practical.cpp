// CJZ.Exam2-Practical

#include <iostream>
#include <conio.h>
#include <fstream>
#include <string>

using namespace std;
const int numCount = 5;
float numbers[numCount];

//Collect and store all the numbers in an array
void collect(float number[])
{
    for (int i = 0; i < numCount; i++) {
        cout << "Enter Number " << i+1 << ": ";
        cin >> number[i];
    }
}

//Display all the numbers and the average
void display(float number[], float avg, float min, float max)
{
    cout << "The numbers you entered are: ";
    for (int i = 0; i < numCount; i++)
    {
        cout << number[i] << " ";
    }
    cout << "\n";
    cout << "The reverse order of the numbers is: ";
    for (int i = numCount; i > 0; i--)
    {
        cout << number[i-1] << " ";
    }
    cout << "\n";
    cout << "The average of these numbers is: " << avg;
    cout << "\n";
    cout << "The minimum of these numbers is: " << min;
    cout << "\n";
    cout << "The maximum of these numbers is: " << max;
}

float FindAverage(float number[])
{
    //Set variable to store the running total
    float running = 0;
    //Add all the numbers
    for (int i = 0; i < numCount; i++) {
        running = running + number[i];
    }
    //Divide by the numCount
    running = running / numCount;
    return running;
}

float FindMin(float number[])
{
    //Set variable to store the running number
    float running = number[0];
    //If the next number is smaller, set it to running
    for (int i = 0; i < numCount; i++)
    {
        if (number[i] < running)
        {
            running = number[i];
        }
    }
    //Return the running
    return running;
}

float FindMax(float number[])
{
    //Set variable to store the running number
    float running = number[0];
    //If the next number is larger, set it to running
    for (int i = 0; i < numCount; i++)
    {
        if (number[i] > running)
        {
            running = number[i];
        }
    }
    //Return the running
    return running;
}

int main()
{
    cout << "Please enter five numbers." << "\n";
    collect(numbers);
    cout << "\n";
    display(numbers, FindAverage(numbers), FindMin(numbers), FindMax(numbers));
    //cout << composeResults(numbers, FindAverage(numbers), FindMin(numbers), FindMax(numbers));

    cout << "\n";
    cout << "\n";

    cout << "Would you like to save your results? [Y for yes, N for no] ";
    char choice;
    cin >> choice;
    if (choice == 'y')
    {
        string path = "Results.txt";
        ofstream ofs(path);

        ofs << "The numbers you entered are: ";
        for (int i = 0; i < numCount; i++)
        {
            ofs << numbers[i] << " ";
        }
        ofs << "\n";
        ofs << "The reverse order of the numbers is: ";
        for (int i = numCount; i > 0; i--)
        {
            ofs << numbers[i - 1] << " ";
        }
        ofs << "\n";
        ofs << "The average of these numbers is: " << FindAverage(numbers);
        ofs << "\n";
        ofs << "The minimum of these numbers is: " << FindMin(numbers);
        ofs << "\n";
        ofs << "The maximum of these numbers is: " << FindMax(numbers);

        ofs.close();

        cout << "Your results are saved!";

    }
    else
    {
        cout << "Your results will not be saved.";
    }
    
    _getch();
    return 0;
}

